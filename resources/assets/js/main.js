var main = function() {
    var ScrollableContainers = document.getElementsByClassName('einu-scrollable');
    var ScrollableContainersLength = ScrollableContainers.length;
    if (ScrollableContainersLength) {
        for (var i = 0; i < ScrollableContainersLength; i++) {
            window.EinuScrollbar.initialize(ScrollableContainers[i]);
        }
    }
    $(window).resize(function() {
        if (ScrollableContainersLength) {
            for (i = 0; i < ScrollableContainersLength; i++) {
                window.EinuScrollbar.update(ScrollableContainers[i]);
            }
        }
    });
};

$(document).ready(main);