<einu-nav>
    <a class="{{ $__env->yieldContent('subnav_active') == 'overview' ? 'active' : '' }}" href="{{ route('wallet.index') }}">Overview</a>
    <a class="{{ $__env->yieldContent('subnav_active') == 'transactions' ? 'active' : '' }}" href="{{ route('wallet.transactions') }}">Transactions</a>
    <a class="{{ $__env->yieldContent('subnav_active') == 'exchange' ? 'active' : '' }}" href="{{ route('wallet.exchange') }}">Exchange</a>
    <a class="{{ $__env->yieldContent('subnav_active') == 'deposit' ? 'active' : '' }}" href="{{ route('wallet.deposit') }}">Deposit</a>
    <a class="{{ $__env->yieldContent('subnav_active') == 'send' ? 'active' : '' }}" href="{{ route('wallet.send') }}">Send</a>
</einu-nav>