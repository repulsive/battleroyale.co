@extends('layouts.app')
@section('sidebar_active', 'wallet')
@section('subnav_active', 'send')

@section('content')
    <einu-section class="wallet">
        @include('wallet.layouts.navigation')
        <einu-heading class="section-heading">Send</einu-heading>

        <einu-grid>
            <einu-col size="M12 T12 D12">
                <einu-block class="section">
                    <einu-block class="address">
                        <einu-col size="M12 T12 D6" offset="M0 T0 D3">
                            <form class="einu-form" method="POST" action="{{ route('wallet.send') }}">
                                {{ csrf_field() }}

                                <einu-control-group>
                                    <label for="address">Receiver's address</label>
                                    <input type="text" id="address" name="address" style="font-family: 'Source Code Pro';" />
                                    <einu-textblock class="notice red">
                                        WARNING: If you misspell the address you will lose whole amount of funds you entered below.
                                    </einu-textblock>
                                </einu-control-group>
                                <einu-control-group>
                                    <einu-col size="M9 T9 D9">
                                        <label for="amount">Amount</label>
                                        <input type="text" id="amount" name="amount" />
                                    </einu-col>
                                    <einu-col size="M3 T3 D3">
                                        <label for="currency">Currency</label>
                                        <select name="currency" id="currency">
                                            <option value="BRP" selected="selected">BRP</option>
                                            <option value="BRC">BRC</option>
                                        </select>
                                    </einu-col>
                                </einu-control-group>
                                <einu-control-group>
                                    <einu-textblock class="notice right">
                                        Send fee is: <span class="yellow fee">{{ $__settings->send_fee }} BRP</span>
                                    </einu-textblock>
                                </einu-control-group>
                                <einu-control-group>
                                    <button class="button" type="Submit">Send</button>
                                </einu-control-group>
                            </form>
                        </einu-col>
                    </einu-block>
                </einu-block>
            </einu-col>
        </einu-grid>
    </einu-section>
@endsection

@push('scripts')
<script>
    function change_fee() {
        var c = $("#amount").val();
        var a = $("#currency").val();

        switch (a) {
            case 'BRC':
                $(".fee").html(({{ $__settings->send_fee }} / {{ $__settings->exchange_rate }}) + ' ' + a);
                break;

            case 'BRP':
                $(".fee").html({{ $__settings->send_fee }} + ' ' + a) ;
                break;
        }
    }
    $("#currency").change(function() {
        switch ($(this).val()) {
            case "BRC":
                change_fee();
                break;
            case "BRP":
                change_fee();
                break;
        }
    });
</script>
@endpush