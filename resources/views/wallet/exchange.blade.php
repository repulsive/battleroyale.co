@extends('layouts.app')
@section('sidebar_active', 'wallet')
@section('subnav_active', 'exchange')

@section('content')
    <einu-section class="wallet">
        @include('wallet.layouts.navigation')
        <einu-heading class="section-heading">Exchange</einu-heading>

        <einu-grid>
            <einu-col size="M12 T12 D12">
                <einu-block class="section">
                    <einu-block class="address">
                        <einu-col size="M12 T12 D6" offset="M0 T0 D3">
                            <form class="einu-form" method="POST" action="{{ route('wallet.exchange') }}">
                                {{ csrf_field() }}

                                <einu-control-group>
                                    <einu-col size="M9 T9 D9">
                                        <label for="from_amount">From Amount</label>
                                        <input type="text" id="from_amount" name="from_amount" />
                                    </einu-col>
                                    <einu-col size="M3 T3 D3">
                                        <label for="from_currency">Currency</label>
                                        <select name="from_currency" id="from_currency">
                                            <option value="BRP" selected="selected">BRP</option>
                                            <option value="BRC">BRC</option>
                                        </select>
                                    </einu-col>
                                </einu-control-group>

                                <einu-control-group>
                                    <einu-col size="M9 T9 D9">
                                        <label for="to_amount">To Amount</label>
                                        <input type="text" id="to_amount" name="to_amount" disabled />
                                    </einu-col>
                                    <einu-col size="M3 T3 D3">
                                        <label for="to_currency">Currency</label>
                                        <select name="to_currency" id="to_currency" disabled>
                                            <option value="BRP">BRP</option>
                                            <option value="BRC" selected="selected">BRC</option>
                                        </select>
                                    </einu-col>
                                </einu-control-group>
                                <einu-control-group>
                                    <einu-textblock class="notice right">
                                        Exchange fee is: <span class="yellow fee">{{ $__settings->exchange_fee }} BRP</span>
                                    </einu-textblock>
                                </einu-control-group>
                                <einu-control-group>
                                    <button class="button" type="Submit">Exchange</button>
                                </einu-control-group>
                            </form>
                        </einu-col>
                    </einu-block>
                </einu-block>
            </einu-col>
        </einu-grid>
    </einu-section>
@endsection

@push('scripts')
<script>
    function exchange() {
        var c = $("#from_amount").val();
        var a = $("#from_currency").val();

        switch (a) {
            case 'BRC':
                $("#to_amount").val(c * {{ $__settings->exchange_rate }});
                $(".fee").html(({{ $__settings->exchange_fee }} / {{ $__settings->exchange_rate }}) + ' ' + a);
                break;

            case 'BRP':
                $("#to_amount").val(c / {{ $__settings->exchange_rate }});
                $(".fee").html({{ $__settings->exchange_fee }} + ' ' + a) ;
                break;
        }
    }
    $("#from_amount").keyup(function() {
        exchange();
    }), $("#from_currency").change(function() {
        var a = $(this).val();

        switch (a) {
            case "BRC":
                $("#to_currency").val("BRP");
                exchange();
                break;
            case "BRP":
                $("#to_currency").val("BRC");
                exchange();
                break;
        }
    }), $("#to_currency").change(function() {
        var a = $(this).val();

        switch (a) {
            case "BRC":
                $("#from_currency").val("BRP");
                exchange();
                break;
            case "BRP":
                $("#from_currency").val("BRC");
                exchange();
                break;
        }
    });
</script>
@endpush