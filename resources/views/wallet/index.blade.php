@extends('layouts.app')
@section('sidebar_active', 'wallet')
@section('subnav_active', 'overview')

@section('content')
    <einu-section class="wallet">
        @include('wallet.layouts.navigation')
        <einu-heading class="section-heading">Wallet</einu-heading>

        <einu-grid>
            <einu-col size="M12 T12 D12">
                <einu-block class="section">
                    <einu-heading size="1">Address</einu-heading>

                    <einu-block class="address">
                        Your wallet address:
                        <span class="address">{{ $address->address }}</span>
                    </einu-block>
                </einu-block>
            </einu-col>

            <einu-col size="M12 T12 D6">
                <einu-block class="section">
                    <einu-heading size="1">Points</einu-heading>

                    <einu-block class="balance">
                        <span class="value">{{ number_format($points->amount, 2) }}</span>
                        <span class="currency">BRP</span>
                    </einu-block>
                </einu-block>
            </einu-col>
            <einu-col size="M12 T12 D6">
                <einu-block class="section">
                    <einu-heading size="1">Coins</einu-heading>

                    <einu-block class="balance">
                        <span class="value">{{ number_format($coins->amount, 2) }}</span>
                        <span class="currency">BRC</span>
                    </einu-block>
                </einu-block>
            </einu-col>
        </einu-grid>
    </einu-section>
@endsection