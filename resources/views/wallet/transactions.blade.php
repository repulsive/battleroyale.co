@extends('layouts.app')
@section('sidebar_active', 'wallet')
@section('subnav_active', 'transactions')

@section('content')
    <einu-section class="wallet">
        @include('wallet.layouts.navigation')
        <einu-heading class="section-heading">Transactions</einu-heading>

        <einu-grid>
            <einu-col size="M12 T12 D12">
                <einu-block class="section">
                    <einu-table>
                        <einu-table-header>
                            <einu-table-cell>ID</einu-table-cell>
                            <einu-table-cell>Date</einu-table-cell>
                            <einu-table-cell>Amount</einu-table-cell>
                            <einu-table-cell>Balance</einu-table-cell>
                            <einu-table-cell>Type</einu-table-cell>
                            <einu-table-cell>Fee</einu-table-cell>
                            <einu-table-cell>Comment</einu-table-cell>
                        </einu-table-header>
                        @foreach ($transactions as $transaction)
                            <einu-table-row>
                                <einu-table-cell>{{ $transaction->id }}</einu-table-cell>
                                <einu-table-cell>{{ $transaction->created_at }}</einu-table-cell>
                                <einu-table-cell class="amount">
                                    {!!
                                        $transaction->direction
                                        ? '<span class="positive"><einu-icon code="plus"></einu-icon>'.$transaction->amount.' '.$transaction->currency.'</span>'
                                        : '<span class="negative"><einu-icon code="minus"></einu-icon>'.$transaction->amount.' '.$transaction->currency.'</span>'
                                    !!}
                                </einu-table-cell>
                                <einu-table-cell class="balance">{{ $transaction->balance.' '.$transaction->currency }} </einu-table-cell>
                                <einu-table-cell class="type">
                                    <einu-icon code="{{ $transaction->type->icon_code }}"></einu-icon> {{ $transaction->type->name }}
                                </einu-table-cell>
                                <einu-table-cell>{{ $transaction->fee.' '.$transaction->currency }}</einu-table-cell>
                                <einu-table-cell class="comment">{!! $transaction->comment !!}</einu-table-cell>
                            </einu-table-row>
                        @endforeach
                    </einu-table>
                </einu-block>
            </einu-col>
            <einu-col size="M12 T12 D12" style="padding-top: 0; padding-bottom: 0;">
                {{ $transactions->links() }}
            </einu-col>
        </einu-grid>
    </einu-section>
@endsection