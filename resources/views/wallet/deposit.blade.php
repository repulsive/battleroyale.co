@extends('layouts.app')
@section('sidebar_active', 'wallet')
@section('subnav_active', 'deposit')

@section('content')
    <einu-section class="wallet">
        @include('wallet.layouts.navigation')
        <einu-heading class="section-heading">Deposit</einu-heading>
    </einu-section>
@endsection