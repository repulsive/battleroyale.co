@extends('layouts.app')
@section('topbar_active', 'forum')

@section('content')
    <einu-section class="forum">
        <einu-heading class="section-heading">{{ $thread->forum->category->title }} / {{ $thread->forum->title }} / {{ $thread->title }}</einu-heading>

        <einu-grid>
            <einu-col size="M12 T12 D12" style="padding-top: 0; padding-bottom: 0;">
                {{ $replies->links() }}
                @if (auth()->user()->roles->first()->level > 2)
                    <a style="float: right; background-color: #E69800;" class="button" href="{{ route('forum.thread.feature', $thread->id.'-'.str_slug($thread->title, '-')) }}">
                        <einu-icon code="star"></einu-icon>
                        {{ $thread->featured ? 'Featured' : 'Feature' }}
                    </a>
                    <a style="float: right; background-color: #E69800;" class="button" href="{{ route('forum.thread.pin', $thread->id.'-'.str_slug($thread->title, '-')) }}">
                        <einu-icon code="thumb-tack"></einu-icon>
                        {{ $thread->pinned ? 'Pinned' : 'Pin' }}
                    </a>
                    <a style="float: right; background-color: #E69800;" class="button" href="{{ route('forum.thread.lock', $thread->id.'-'.str_slug($thread->title, '-')) }}">
                        <einu-icon code="lock"></einu-icon>
                        {{ $thread->locked ? 'Locked' : 'Lock' }}
                    </a>
                    <form method="POST" action="{{ route('forum.thread', $thread->id.'-'.str_slug($thread->title, '-')) }}">
                        {{ csrf_field() }}
                        {{ method_field('DELETE') }}

                        <button type="submit" class="button" style="float: right; background-color: #C02C44;">
                            <einu-icon code="trash-o"></einu-icon>
                            Junk
                        </button>
                    </form>
                    <a style="float: right; background-color: #C02C44;" class="button" href="">
                        <einu-icon code="arrows"></einu-icon>
                        Move
                    </a>
                @endif
            </einu-col>

            @foreach ($replies as $reply)
                <einu-col size="M12 T12 D12" id="reply-{{ $reply->id }}">
                    <einu-block class="section thread-reply">
                        <einu-block class="user-info" style="background-image: url('{{ asset($reply->creator->level->rank->background_path) }}');">
                            <einu-block class="username">
                                <span class="role-{{ strtolower($reply->creator->roles->first()->name) }}">
                                    <a href="{{ route('profile', '@'.$reply->creator->username) }}">{{ $reply->creator->username }}</a>
                                </span>
                            </einu-block>
                            <einu-block class="role">
                                {{ $reply->creator->roles->first()->name }}
                            </einu-block>
                            <einu-block class="avatar"
                                        style="box-shadow: 0 0 30px {{ $reply->creator->level->rank->color }}; background-image: url('{{ asset('storage/avatars/'.$reply->creator->avatar) }}');">
                            </einu-block>
                            <einu-block class="level">
                                {{ $reply->creator->level->rank->name }} [{{ $reply->creator->level->level }}]
                            </einu-block>
                        </einu-block>
                        <einu-block class="reply-content">
                            <einu-block class="reply-header">
                                <span class="date">
                                    {{ \Carbon\Carbon::createFromTimeStamp(strtotime($reply->created_at))->diffForHumans() }}
                                </span>
                                <span class="options">
                                    <a href="#"><einu-icon code="share-alt"></einu-icon></a>
                                    <a href="#"><einu-icon code="flag"></einu-icon> Report</a>
                                </span>
                            </einu-block>
                            <einu-block class="reply">
                                {{ $reply->content }}
                            </einu-block>
                            @if ($reply->creator->forum_signature != null)
                                <einu-block class="signature">
                                    {{ $reply->creator->forum_signature->content }}
                                </einu-block>
                            @endif
                        </einu-block>
                    </einu-block>
                </einu-col>
            @endforeach

            <einu-col size="M12 T12 D12" style="padding-top: 0; padding-bottom: 0;">
                {{ $replies->links() }}
            </einu-col>
        </einu-grid>

        @if (!$thread->locked)
            <einu-heading class="section-heading">Reply to this thread</einu-heading>
            <einu-grid>
                <einu-col size="M12 T12 D12">
                    <einu-block class="section">
                        <einu-heading size="1">New reply</einu-heading>
                        <form class="einu-form" method="POST" action="{{ route('forum.thread', $thread->id) }}">
                            {{ csrf_field() }}

                            <einu-control-group>
                                <label for="content">Reply content</label>
                                <textarea rows="6" name="content" id="content"></textarea>
                            </einu-control-group>

                            <einu-control-group>
                                <button class="button" type="submit">Reply</button>
                            </einu-control-group>
                        </form>
                    </einu-block>
                </einu-col>
            </einu-grid>
        </einu-section>
    @endif
@endsection