@extends('layouts.app')
@section('topbar_active', 'forum')

@section('content')
    <einu-section class="forum">
        <einu-heading class="section-heading">{{ $forum->category->title }} / {{ $forum->title }}</einu-heading>

        <einu-grid>
            <einu-col size="M12 T12 D12" style="padding-top: 0; padding-bottom: 0;">
                {{ $threads->links() }}
                <a style="float: right; background-color: #E69800;" class="button" href="{{ route('forum.new', $forum->id) }}">
                    <einu-icon code="plus"></einu-icon>
                    New thread
                </a>
                <a style="float: right; background-color: #C02C44;" class="button" href="{{ route('forum.new', $forum->id) }}">
                    <einu-icon code="pencil"></einu-icon>
                    Edit forum
                </a>
            </einu-col>

            <einu-col size="M12 T12 D12">
                <einu-block class="section">
                    <einu-heading size="1">{{ $forum->title }}</einu-heading>
                    <einu-table>
                        @foreach ($threads as $thread)
                            <einu-table-row>
                                <einu-table-cell>
                                    <einu-block class="info">
                                        <einu-block class="title">
                                            {!! $thread->locked ? '<span class="thread-status-icon"><einu-icon code="lock"></einu-icon></span>' : '' !!}
                                            {!! $thread->pinned ? '<span class="thread-status-icon"><einu-icon code="thumb-tack"></einu-icon></span>' : '' !!}
                                            {!! $thread->featured ? '<span class="thread-status-icon"><einu-icon code="star"></einu-icon></span>' : '' !!}
                                            <a href="{{ route('forum.thread', $thread->id.'-'.str_slug($thread->title, '-')) }}">{{ $thread->title }}</a>
                                        </einu-block>
                                        <einu-block class="sub-title">
                                            By <a href="{{ route('profile', '@'.$thread->creator->username) }}">{{ $thread->creator->username }}</a>,
                                            {{ \Carbon\Carbon::createFromTimeStamp(strtotime($thread->created_at))->diffForHumans() }}
                                        </einu-block>
                                    </einu-block>
                                </einu-table-cell>
                                <einu-table-cell class="stats_cell">
                                    <einu-block class="stats">
                                        <einu-block>
                                            {{ $thread->reply_count }} {{ $thread->reply_count == 1 ? 'reply' : 'replies' }}
                                        </einu-block>
                                        <einu-block>
                                            {{ $thread->views }} {{ $thread->views == 1 ? 'view' : 'views' }}
                                        </einu-block>
                                    </einu-block>
                                </einu-table-cell>
                                <einu-table-cell class="last_reply_cell">
                                    <einu-block class="last_reply">
                                        <einu-block class="avatar">
                                            <a href="{{ route('profile', '@'.$thread->replies->last()->creator->username) }}">
                                                <img src="{{ asset('storage/avatars/'.$thread->replies->last()->creator->avatar) }}"
                                                    alt="{{ $thread->replies->last()->creator->username }}" />
                                            </a>
                                        </einu-block>
                                        <einu-block class="user">
                                            <einu-block>
                                                <span class="role-{{ strtolower($thread->replies->last()->creator->roles->first()->name) }}">
                                                    <a href="{{ route('profile', '@'.$thread->replies->last()->creator->username) }}">{{ $thread->replies->last()->creator->username }}</a>
                                                </span>
                                            </einu-block>
                                            <einu-block class="date"><a href="{{ route('forum.thread', $thread->id) }}?page={{ $thread->last_reply_page }}#reply-{{ $thread->replies->last()->id }}">{{ \Carbon\Carbon::createFromTimeStamp(strtotime($thread->replies->last()->created_at))->diffForHumans() }}</a></einu-block>
                                        </einu-block>
                                    </einu-block>
                                </einu-table-cell>
                            </einu-table-row>
                        @endforeach
                    </einu-table>
                </einu-block>
            </einu-col>

            <einu-col size="M12 T12 D12" style="padding-top: 0; padding-bottom: 0;">
                {{ $threads->links() }}
                <a style="float: right; background-color: #E69800;" class="button" href="{{ route('forum.new', $forum->id) }}">
                    <einu-icon code="plus"></einu-icon>
                    New thread
                </a>
                <a style="float: right; background-color: #C02C44;" class="button" href="{{ route('forum.new', $forum->id) }}">
                    <einu-icon code="pencil"></einu-icon>
                    Edit forum
                </a>
            </einu-col>
        </einu-grid>
    </einu-section>
@endsection