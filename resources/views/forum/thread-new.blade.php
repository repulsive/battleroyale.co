@extends('layouts.app')
@section('topbar_active', 'forum')

@section('content')
    <einu-section class="forum">
        <einu-heading class="section-heading">{{ $forum->category->title }} / {{ $forum->title }}</einu-heading>

        <einu-grid>
            <einu-col size="M12 T12 D12">
                <einu-block class="section">
                    <einu-heading size="1">New thread</einu-heading>
                    <form class="einu-form" method="POST" action="{{ route('forum.new', $forum->id) }}">
                        {{ csrf_field() }}

                        <einu-control-group>
                            <label for="title">Thread title</label>
                            <input type="text" name="title" id="title" />
                        </einu-control-group>

                        <einu-control-group>
                            <label for="content">Thread content</label>
                            <textarea rows="6" name="content" id="content"></textarea>
                        </einu-control-group>

                        @if (auth()->user()->roles->first()->level > 2)
                            <einu-control-group>
                                <einu-block style="padding: 0;">
                                    <input type="checkbox" name="locked" id="locked" value="1" />
                                    <label for="locked">Locked thread</label>
                                </einu-block>
                                <einu-block style="padding: 0;">
                                    <input type="checkbox" name="pinned" id="pinned" value="1" />
                                    <label for="pinned">Pinned thread</label>
                                </einu-block>
                                <einu-block style="padding: 0;">
                                    <input type="checkbox" name="featured" id="featured" value="1" />
                                    <label for="featured">Featured thread</label>
                                </einu-block>
                            </einu-control-group>
                        @endif

                        <einu-control-group>
                            <button class="button" type="submit">Publish</button>
                            <a style="background-color: #C02C44;" class="button" href="{{ route('forum.forum', $forum->id) }}">Back</a>
                        </einu-control-group>
                    </form>
                </einu-block>
            </einu-col>
        </einu-grid>
    </einu-section>
@endsection