@extends('layouts.app')
@section('topbar_active', 'forum')

@section('content')
    <einu-section class="forum">
        <einu-heading class="section-heading">Forums</einu-heading>

        <einu-grid>
            @foreach ($categories as $category)
                <einu-col size="M12 {{ $category->full_width ? "T12 D12" : "T12 D12" }}">
                    <einu-block class="section">
                        <einu-heading size="1">{{ $category->title }}</einu-heading>
                        <einu-table>
                            @foreach ($category->redirects as $redirect)
                                <einu-table-row onclick="window.location='{{ $redirect->link }}';">
                                    <einu-table-cell>{{ $redirect->title }}</einu-table-cell>
                                    <einu-table-cell></einu-table-cell>
                                    <einu-table-cell>Something</einu-table-cell>
                                </einu-table-row>
                            @endforeach
                            @foreach ($category->forums as $forum)
                                <einu-table-row>
                                    <einu-table-cell>
                                        <einu-block class="info">
                                            <einu-block class="title">
                                                <a href="{{ route('forum.forum', $forum->id.'-'.str_slug($forum->title, '-')) }}">{{ $forum->title }}</a>
                                            </einu-block>
                                            <einu-block class="sub-title">
                                                {{ $forum->description }}asdasdasd  
                                            </einu-block>
                                        </einu-block>
                                    </einu-table-cell>
                                    <einu-table-cell class="index-post-count">
                                        <einu-block class="count">
                                            <einu-block>{{ $forum->thread_count }}</einu-block>
                                            <einu-block>Posts</einu-block>
                                        </einu-block>
                                    </einu-table-cell>
                                    <einu-table-cell class="last_reply_cell">
                                        @if ($forum->thread_count > 0)
                                            @if ($forum->threads()->orderBy('updated_at', 'DESC')->first()->last_reply != null)
                                                <einu-block class="last_reply">
                                                    <einu-block class="avatar">
                                                        <a href="{{ route('profile', '@'.$forum->threads()->orderBy('updated_at', 'DESC')->first()->last_reply->creator->username) }}">
                                                            <img src="{{ asset('storage/avatars/'.$forum->threads()->orderBy('updated_at', 'DESC')->first()->last_reply->creator->avatar) }}"
                                                                alt="{{ $forum->threads()->orderBy('updated_at', 'DESC')->first()->last_reply->creator->name }}" />
                                                        </a>
                                                    </einu-block>
                                                    <einu-block class="user">
                                                        <einu-block>
                                                            <a href="{{ route('forum.thread', $forum->threads()->orderBy('updated_at', 'DESC')->first()->last_reply->thread->id) }}">
                                                                {{ $forum->threads()->orderBy('updated_at', 'DESC')->first()->title }}
                                                            </a>
                                                        </einu-block>
                                                        <einu-block class="date">
                                                            <a href="{{ route('forum.thread', $forum->threads()->orderBy('updated_at', 'DESC')->first()->last_reply->thread->id) }}?page={{ $forum->threads()->orderBy('updated_at', 'DESC')->first()->last_reply->thread->last_reply_page }}#reply-{{ $forum->threads()->orderBy('updated_at', 'DESC')->first()->last_reply->id }}">
                                                                {{ \Carbon\Carbon::createFromTimeStamp(strtotime($forum->threads()->orderBy('updated_at', 'DESC')->first()->last_reply->created_at))->diffForHumans() }}
                                                            </a>
                                                        </einu-block>
                                                    </einu-block>
                                                </einu-block>
                                            @endif
                                        @endif
                                    </einu-table-cell>
                                </einu-table-row>
                            @endforeach
                        </einu-table>
                    </einu-block>
                </einu-col>
            @endforeach
        </einu-grid>
    </einu-section>
    <einu-section class="forum">
        <einu-heading class="section-heading">Informations</einu-heading>

        <einu-grid>
            <einu-col size="M12 T6 D6">
                <einu-block class="section">
                    <einu-heading size="1">Statistics</einu-heading>
                    <einu-table>
                        <einu-table-row></einu-table-row>
                    </einu-table>
                </einu-block>
            </einu-col>
            <einu-col size="M12 T6 D6">
                <einu-block class="section">
                    <einu-heading size="1">Statistics</einu-heading>
                    <einu-table>
                        <einu-table-row>
                            <einu-table-cell >{{ $topUser->forum_replies()->count() }} posts</einu-table-cell>
                        </einu-table-row>
                    </einu-table>
                </einu-block>
            </einu-col>
        </einu-grid>
    </einu-section>
@endsection