<!DOCTYPE html>
    <html lang="{{ app()->getLocale() }}">
    <head>
        {{-- ROOT PASSWORD: |yxGQw6WhcXDbu4 --}}
        {{-- BOT PASSWORDS: c9u17a9m8geq --}}

        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>BattleRoyale.co</title>

        <!-- Styles -->
        <link href="{{ asset('css/app.css') }}" rel="stylesheet" type="text/css">
    </head>
    <body einu-flow="battleroyale.co">
        <einu-app>
            <form action="{{ route('dev-protect') }}" method="POST">
                {{ csrf_field() }}

                <input type="text" name="username" placeholder="username" />
                <input type="password" name="password" placeholder="password" />

                <button type="submit">Authorize</button>
            </form>
        </einu-app>
    </body>
</html>
