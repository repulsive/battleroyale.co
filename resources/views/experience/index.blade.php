@extends('layouts.app')
@section('sidebar_active', 'experience')
@section('subnav_active', 'overview')

@section('content')
    <einu-section class="experience">
        <einu-heading class="section-heading">Experience</einu-heading>

        <einu-grid>
            <einu-col size="M12 T12 D12">
                <einu-block class="section badge" style="background-image: url('{{ asset($rank->background_path) }}');">
                    <einu-block class="align-center">
                        <img class="badge" height="300" src="{{ asset($rank->badge_path) }}" />
                    </einu-block>
                    {{--<progress value="30" max="100"></progress>--}}
                    <einu-block class="align-center rank">
                        <span class="name">{{ $rank->name }}</span>
                        <span class="level">{{ $level->level }}</span>
                    </einu-block>
                </einu-block>
            </einu-col>
        </einu-grid>
    </einu-section>
@endsection