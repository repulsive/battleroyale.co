<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    {{-- ROOT PASSWORD: |yxGQw6WhcXDbu4 --}}
    {{-- BOT PASSWORDS: c9u17a9m8geq --}}

    {{----------------------------------------------------------------------}}
    {{--I M P O R T A N T --}}
    {{----------------------------------------------------------------------}}
    {{-- Server Query Admin Account created --}}
    {{-- loginname= "serveradmin", password= "m8v2R9E3" --}}
    {{----------------------------------------------------------------------}}

    {{----------------------------------------------------------------------}}
    {{-- I M P O R T A N T --}}
    {{----------------------------------------------------------------------}}
    {{-- ServerAdmin privilege key created, please use it to gain --}}
    {{-- serveradmin rights for your virtualserver. please --}}
    {{-- also check the doc/privilegekey_guide.txt for details. --}}

    {{-- token=5l+0tIVbxHvDPBIFITlAqszi8f7asJIEu3ZcHZvF --}}
    {{----------------------------------------------------------------------}}


    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>BattleRoyale.co</title>

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet" type="text/css">
</head>
<body einu-flow="battleroyale.co">
<einu-app>
    <einu-block class="topbar">
        <a class="button logo squared" href="#"></a>

        <einu-block class="search">
            <form>
                <input type="text" placeholder="Search"/>
            </form>
        </einu-block>

        <a class="button" href="#">News</a>
        <a class="button" href="#">Forum</a>
        <a class="button" href="#">Armory</a>
        <a class="button" href="#">Leaderboard</a>
        <a class="button" href="#">Gamble</a>
    </einu-block>
    <einu-block class="sidebar-min"></einu-block>
    <einu-block class="sidebar-left einu-scrollable"></einu-block>
    <einu-block class="workspace-container" sidebar="false">
        <einu-block class="workspace einu-scrollable">
            <einu-content>
                <einu-section class="forum">
                    <einu-heading class="section-heading">Forums</einu-heading>

                    <einu-grid>
                        <einu-col size="M12 T6 D6">
                            <einu-block class="section">
                                <einu-heading size="1">Category 1</einu-heading>
                                <einu-table>
                                    <einu-table-row>
                                        <einu-table-cell>
                                            <img src="{{ asset('img/battleroyale.') }}" />
                                        </einu-table-cell>
                                        <einu-table-cell>Name</einu-table-cell>
                                        <einu-table-cell>Thread Count</einu-table-cell>
                                        <einu-table-cell>Last reply</einu-table-cell>
                                    </einu-table-row>
                                    <einu-table-row>
                                        <einu-table-cell>Icon</einu-table-cell>
                                        <einu-table-cell>Name</einu-table-cell>
                                        <einu-table-cell>Thread Count</einu-table-cell>
                                        <einu-table-cell>Last reply</einu-table-cell>
                                    </einu-table-row>
                                    <einu-table-row>
                                        <einu-table-cell>Icon</einu-table-cell>
                                        <einu-table-cell>Name</einu-table-cell>
                                        <einu-table-cell>Thread Count</einu-table-cell>
                                        <einu-table-cell>Last reply</einu-table-cell>
                                    </einu-table-row>
                                    <einu-table-row>
                                        <einu-table-cell>Icon</einu-table-cell>
                                        <einu-table-cell>Name</einu-table-cell>
                                        <einu-table-cell>Thread Count</einu-table-cell>
                                        <einu-table-cell>Last reply</einu-table-cell>
                                    </einu-table-row>
                                </einu-table>
                            </einu-block>
                        </einu-col>
                        <einu-col size="M12 T6 D6">
                            <einu-block class="section">
                                <einu-heading size="1">Category 1</einu-heading>
                                <einu-table>
                                    <einu-table-row>
                                        <einu-table-cell>
                                            <img src="{{ asset('img/battleroyale.') }}" />
                                        </einu-table-cell>
                                        <einu-table-cell>Name</einu-table-cell>
                                        <einu-table-cell>Thread Count</einu-table-cell>
                                        <einu-table-cell>Last reply</einu-table-cell>
                                    </einu-table-row>
                                    <einu-table-row>
                                        <einu-table-cell>Icon</einu-table-cell>
                                        <einu-table-cell>Name</einu-table-cell>
                                        <einu-table-cell>Thread Count</einu-table-cell>
                                        <einu-table-cell>Last reply</einu-table-cell>
                                    </einu-table-row>
                                    <einu-table-row>
                                        <einu-table-cell>Icon</einu-table-cell>
                                        <einu-table-cell>Name</einu-table-cell>
                                        <einu-table-cell>Thread Count</einu-table-cell>
                                        <einu-table-cell>Last reply</einu-table-cell>
                                    </einu-table-row>
                                </einu-table>
                            </einu-block>
                        </einu-col>
                        <einu-col size="M12 T6 D6">
                            <einu-block class="section">
                                <einu-heading size="1">Category 1</einu-heading>
                                <einu-table>
                                    <einu-table-row>
                                        <einu-table-cell>
                                            <img src="{{ asset('img/battleroyale.') }}" />
                                        </einu-table-cell>
                                        <einu-table-cell>Name</einu-table-cell>
                                        <einu-table-cell>Thread Count</einu-table-cell>
                                        <einu-table-cell>Last reply</einu-table-cell>
                                    </einu-table-row>
                                    <einu-table-row>
                                        <einu-table-cell>Icon</einu-table-cell>
                                        <einu-table-cell>Name</einu-table-cell>
                                        <einu-table-cell>Thread Count</einu-table-cell>
                                        <einu-table-cell>Last reply</einu-table-cell>
                                    </einu-table-row>
                                    <einu-table-row>
                                        <einu-table-cell>Icon</einu-table-cell>
                                        <einu-table-cell>Name</einu-table-cell>
                                        <einu-table-cell>Thread Count</einu-table-cell>
                                        <einu-table-cell>Last reply</einu-table-cell>
                                    </einu-table-row>
                                    <einu-table-row>
                                        <einu-table-cell>Icon</einu-table-cell>
                                        <einu-table-cell>Name</einu-table-cell>
                                        <einu-table-cell>Thread Count</einu-table-cell>
                                        <einu-table-cell>Last reply</einu-table-cell>
                                    </einu-table-row>
                                </einu-table>
                            </einu-block>
                        </einu-col>
                    </einu-grid>
                </einu-section>
            </einu-content>
        </einu-block>
        <einu-block class="sidebar einu-scrollable"></einu-block>
    </einu-block>
</einu-app>

<script type="text/javascript" src="{{ asset('js/app.js') }}"></script>
</body>
</html>
