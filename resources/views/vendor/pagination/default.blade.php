@if ($paginator->hasPages())
    <einu-block class="pagination">
        {{-- Previous Page Link --}}
        @if ($paginator->onFirstPage())
            <a class="button squared" href="#"><einu-icon code="chevron-left"></einu-icon></a>
        @else
            <a class="button squared" href="{{ $paginator->previousPageUrl() }}" rel="prev"><einu-icon code="chevron-left"></einu-icon></a>
        @endif

        {{-- Pagination Elements --}}
        @foreach ($elements as $element)
            {{-- "Three Dots" Separator --}}
            @if (is_string($element))
                <a class="button squared" href="#">{{ $element }}</a>
            @endif

            {{-- Array Of Links --}}
            @if (is_array($element))
                @foreach ($element as $page => $url)
                    @if ($page == $paginator->currentPage())
                        <a class="button squared current" href="{{ $url }}">{{ $page }}</a>
                    @else
                        <a class="button squared" href="{{ $url }}">{{ $page }}</a>
                    @endif
                @endforeach
            @endif
        @endforeach

        {{-- Next Page Link --}}
        @if ($paginator->hasMorePages())
            <a class="button squared" href="{{ $paginator->nextPageUrl() }}" rel="next"><einu-icon code="chevron-right"></einu-icon></a>
        @else
            <a class="button squared" href="#"><einu-icon code="chevron-right"></einu-icon></a>
        @endif
    </einu-block>
@endif
