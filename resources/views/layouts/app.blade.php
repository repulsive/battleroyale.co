<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
    <head>
        {{-- ROOT PASSWORD: |yxGQw6WhcXDbu4 --}}
        {{-- BOT PASSWORDS: c9u17a9m8geq --}}

        {{-- /opt/cpanel/ea-php72/root/etc/php.ini --}}

        {{-- git clone https://repulsive@bitbucket.org/einutech/battleroyale.co.git temp
        mv temp/.git code/.git
        rm -rf temp --}}

        {{----------------------------------------------------------------------}}
        {{--I M P O R T A N T --}}
        {{----------------------------------------------------------------------}}
        {{-- Server Query Admin Account created --}}
        {{-- loginname= "serveradmin", password= "m8v2R9E3" --}}
        {{----------------------------------------------------------------------}}

        {{----------------------------------------------------------------------}}
        {{-- I M P O R T A N T --}}
        {{----------------------------------------------------------------------}}
        {{-- ServerAdmin privilege key created, please use it to gain --}}
        {{-- serveradmin rights for your virtualserver. please --}}
        {{-- also check the doc/privilegekey_guide.txt for details. --}}

        {{-- token=5l+0tIVbxHvDPBIFITlAqszi8f7asJIEu3ZcHZvF --}}
        {{----------------------------------------------------------------------}}

        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>BattleRoyale.co</title>

        <!-- Styles -->
        <link href="{{ asset('css/app.css') }}" rel="stylesheet" type="text/css">
    </head>
    <body einu-flow="battleroyale.co">
        <einu-app sidebar-left="{{ auth()->check() ? "true" : "false" }}">
            @include('layouts.topbar')
            @auth
                @include('layouts.sidebar-min')
                @include('layouts.sidebar')
                @include('layouts.donate-button')
            @endauth

            <einu-block class="workspace-container" sidebar="false">
                <einu-block class="workspace einu-scrollable">
                    <einu-content>
                        @yield('content')
                    </einu-content>
                </einu-block>
                <einu-block class="sidebar einu-scrollable">
                    @yield('sidebar-right-content')
                </einu-block>
            </einu-block>
        </einu-app>

        <script type="text/javascript" src="{{ asset('js/app.js') }}"></script>
        @stack('scripts')
    </body>
</html>
