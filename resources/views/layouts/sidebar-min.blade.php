<einu-block class="sidebar-min">
    <einu-block class="separator"></einu-block>
    <a class="button icon wallet {{ $__env->yieldContent('sidebar_active') == 'wallet' ? 'active' : '' }}" href="{{ route('wallet.index') }}"></a>
    <a class="button icon experience {{ $__env->yieldContent('sidebar_active') == 'experience' ? 'active' : '' }}" href="{{ route('experience.index') }}"></a>
    <einu-block class="separator"></einu-block>
    <a class="button icon profile {{ $__env->yieldContent('sidebar_active') == 'profile' ? 'active' : '' }}" href="#"></a>
    <a class="button icon settings {{ $__env->yieldContent('sidebar_active') == 'settings' ? 'active' : '' }}" href="#"></a>
    <einu-block class="separator"></einu-block>
    <a class="button icon help {{ $__env->yieldContent('sidebar_active') == 'help' ? 'active' : '' }}" href="#"></a>
</einu-block>