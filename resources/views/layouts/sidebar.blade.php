<einu-block class="sidebar-left einu-scrollable">
    <einu-block class="user">
        <einu-block class="avatar"
                    style="background-image: url('{{ asset('storage/avatars/'.auth()->user()->avatar) }}');">
        </einu-block>
        <einu-block class="info">
            <einu-block class="line username">
                <a href="{{route('profile', '@'.auth()->user()->username) }}">{{ auth()->user()->username }}</a>
            </einu-block>
            <einu-block class="line logout">
                <a href="{{ route('auth.logout') }}">
                    <einu-icon code="sign-out"></einu-icon>
                    Logout
                </a>
            </einu-block>
        </einu-block>
    </einu-block>
    @yield('sidebar-left-content')
</einu-block>