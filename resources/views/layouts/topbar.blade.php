<einu-block class="topbar">
    <a class="button logo squared" href="#"></a>

    <einu-block class="search">
        <form>
            <input type="text" placeholder="Search"/>
        </form>
    </einu-block>

    <einu-block class="navigation">
        <a class="button {{ $__env->yieldContent('topbar_active') == 'news' ? 'active' : '' }}" href="#">News</a>
        <a class="button {{ $__env->yieldContent('topbar_active') == 'forum' ? 'active' : '' }}" href="{{ route('forum.index') }}">Forum</a>
        <a class="button {{ $__env->yieldContent('topbar_active') == 'armory' ? 'active' : '' }}" href="#">Armory</a>
        <a class="button {{ $__env->yieldContent('topbar_active') == 'leaderboard' ? 'active' : '' }}" href="#">Leaderboard</a>
        <a class="button {{ $__env->yieldContent('topbar_active') == 'games' ? 'active' : '' }}" href="#">Games</a>
    </einu-block>
    @auth
        <einu-block class="wallet">
            <span>{{ number_format(auth()->user()->wallet_points->amount, 2) }} <span class="currency">BRP</span></span>
            <span>{{ number_format(auth()->user()->wallet_coins->amount, 2) }} <span class="currency">BRC</span></span>
        </einu-block>
    @endauth
</einu-block>