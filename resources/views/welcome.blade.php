<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
    <head>
        {{-- ROOT PASSWORD: |yxGQw6WhcXDbu4 --}}
        {{-- BOT PASSWORDS: c9u17a9m8geq --}}

        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>BattleRoyale.co</title>

        <!-- Styles -->
        <link href="{{ asset('css/app.css') }}" rel="stylesheet" type="text/css">
    </head>
    <body einu-flow="battleroyale.co">
        <einu-app>
// topbar
            // sidebarmin
            // sidebar


            <einu-block class="workspace-container" sidebar="false">
                <einu-block class="workspace einu-scrollable">
                    <einu-content>

                    </einu-content>
                </einu-block>
                <einu-block class="sidebar einu-scrollable"></einu-block>
            </einu-block>
        </einu-app>

        <script type="text/javascript" src="{{ asset('js/app.js') }}"></script>
    </body>
</html>
