@extends('layouts.app')
@section('topbar_active', 'news')

@section('content')
    <einu-section class="news">
        <einu-heading class="section-heading">Categories</einu-heading>
        <einu-container>
            <einu-grid class="categories">
                <einu-col size="M6 T4 D2">
                    <a class="button category" href="#" style="background-color: #c02e0e;">PUBG</a>
                </einu-col>
                <einu-col size="M6 T4 D2">
                    <a class="button category" href="#" style="background-color: #22637f;">Fortnite</a>
                </einu-col>
                <einu-col size="M6 T4 D2">
                    <a class="button category" href="#" style="background-color: #d5d20c;">Guides</a>
                </einu-col>
                <einu-col size="M6 T4 D2">
                    <a class="button category" href="#" style="background-color: #5bd073;">Esports</a>
                </einu-col>
            </einu-grid>
        </einu-container>


        <einu-heading class="section-heading">Most recent posts</einu-heading>
        <einu-container>
            <einu-grid>
                @for ($i = 0; $i < 6; $i++)
                    <einu-col size="M12 T6 D6 X4">
                        <einu-article class="large" style="background-image: url('{{ asset('img/article1.jpg') }}');">
                            <einu-heading size="2">Lorem ipsum dolor sit amet</einu-heading>
                            <einu-textblock>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed et odio tristique, consequat risus nec, lacinia enim. In et velit lacus. Cras accumsan velit ut risus vulputate, at rutrum nisl porttitor.</einu-textblock>
                            <einu-block class="info">
                                <span><einu-icon code="eye"></einu-icon> 1678</span>
                                <span><einu-icon code="comments"></einu-icon> 253</span>
                            </einu-block>
                        </einu-article>
                    </einu-col>
                @endfor
            </einu-grid>
        </einu-container>
    </einu-section>
@endsection