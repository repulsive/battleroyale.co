<!DOCTYPE html>
<html>
<head>
    <link type="text/css" href="{{ asset('css/app.css') }}" rel="stylesheet" />

    <title>Repulsive.xyz</title>

    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta charset="UTF-8" />

    <meta name="description" content="" />
    <meta name="keywords" content="einutech,web,dizajn,design,izrada,sajtovi,jovan,ivanovic" />
    <meta name="author" content="Repulsive" />
    <meta name="language" content="en" />

    <meta property="og:title" content="Repulsive (Jovan Ivanovi?)" />
    <meta property="og:site_name" content="Repulsive.xyz" />
    <meta property="og:image" content="http://www.repulsive.xyz/ProfilePic.png" />

    <meta property="og:description" content="Einutech is your best choice in the sphere of web development. If you have a plan, a vision, an idea, we are here to bring it to life! We design any kind of website you desire, fully customizing it for our customers and future users for the best experience on the market. You are the architect and we are the builders." />
</head>
<body einu-flow="flow-hub" container class="repulsive">
    <einu-block class="profile-parent">
        <einu-heading size="1" class="title">Repulsive<span>.</span><span class="x">x</span><span class="y">y</span><span class="z">z</span></einu-heading>
        <einu-block>
            <einu-block class="profile">
                <einu-label>Jovan Ivanovi?</einu-label>
                <einu-label caption>CEO & Founder � einutech</einu-label>
                <img class="pulse-image" src="ProfilePic.png" />
                <a href="mailto:jovanivanovic@einutech.com">
                    <einu-icon code="paper-plane" fixed-width></einu-icon>
                    Contact Me
                </a>
                <einu-separator></einu-separator>
                <a target="_blank" class="social fb" href="https://www.facebook.com/neosrb.skynix">
                    <einu-icon code="facebook" fixed-width></einu-icon>
                </a>
                <a target="_blank" class="social tw" href="https://twitter.com/RepulsiveJohn">
                    <einu-icon code="twitter" fixed-width></einu-icon>
                </a>
                <a target="_blank" class="social li" href="https://rs.linkedin.com/in/jovanivanovic">
                    <einu-icon code="linkedin" fixed-width></einu-icon>
                </a>
            </einu-block>
            <a target="_blank" href="http://www.einutech.com/">
                <img class="einutech-logo" src="http://einutech.com/public/img/logo-small.png" width="193" height="50" />
            </a>
        </einu-block>
    </einu-block>
    <script type="text/javascript" src="flow/einutech.flow.js"></script>
</body>
</html>
