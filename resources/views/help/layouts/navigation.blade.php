<einu-nav>
    <a class="{{ $__env->yieldContent('subnav_active') == 'support' ? 'active' : '' }}" href="{{ route('help.support') }}">Support</a>
    <a class="{{ $__env->yieldContent('subnav_active') == 'contact' ? 'active' : '' }}" href="{{ route('help.contact') }}">Contact</a>
    <a class="{{ $__env->yieldContent('subnav_active') == 'faq' ? 'active' : '' }}" href="{{ route('help.faq') }}">FAQ</a>
    <a class="{{ $__env->yieldContent('subnav_active') == 'ranks' ? 'active' : '' }}" href="{{ route('help.ranks') }}">Ranks</a>
    <a class="{{ $__env->yieldContent('subnav_active') == 'levels' ? 'active' : '' }}" href="{{ route('help.levels') }}">Levels</a>
</einu-nav>