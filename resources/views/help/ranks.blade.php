@extends('layouts.app')
@section('sidebar_active', 'help')
@section('subnav_active', 'ranks')

@section('content')
    <einu-section class="help">
        @include('help.layouts.navigation')
        <einu-heading class="section-heading">Ranks</einu-heading>

        <einu-grid>
            @foreach ($ranks as $rank)
                <einu-col id="{{ str_slug($rank->name) }}" size="{{ $rank->name == 'Sentinel' ? 'M12 T12 D12' : 'M12 T12 D4' }}">
                    <einu-block class="section rank">
                        <einu-block class="background" style="background-image: url('{{ asset($rank->background_path) }}');">
                            <einu-heading style="background: transparent;" size="1">{{ $rank->name }}</einu-heading>

                            <img class="badge" src="{{ asset($rank->badge_path) }}" />
                            <einu-table>
                                <einu-table-row>
                                    <einu-table-cell>Required level</einu-table-cell>
                                    <einu-table-cell>{{ $rank->required_level->level }}</einu-table-cell>
                                </einu-table-row>
                                <einu-table-row>
                                    <einu-table-cell>Daily points</einu-table-cell>
                                    <einu-table-cell>{{ number_format($rank->required_level->daily_points, 2) }} BRP</einu-table-cell>
                                </einu-table-row>
                            </einu-table>
                        </einu-block>
                    </einu-block>
                </einu-col>
            @endforeach
        </einu-grid>
    </einu-section>
@endsection