@extends('layouts.app')
@section('sidebar_active', 'help')
@section('subnav_active', 'levels')

@section('content')
    <einu-section class="help">
        @include('help.layouts.navigation')
        <einu-heading class="section-heading">Levels</einu-heading>

        <einu-grid>
            <einu-col size="M12 T12 D12">
                <einu-block class="section">
                    <einu-table>
                        <einu-table-header>
                            <einu-table-cell>Level</einu-table-cell>
                            <einu-table-cell>Rank</einu-table-cell>
                            <einu-table-cell>Daily points</einu-table-cell>
                        </einu-table-header>
                        @foreach ($levels as $level)
                            <einu-table-row>
                                <einu-table-cell>{{ $level->level }}</einu-table-cell>
                                <einu-table-cell>{{ $level->rank->name }}</einu-table-cell>
                                <einu-table-cell>{{ number_format($level->daily_points, 0) }} BRP</einu-table-cell>
                            </einu-table-row>
                        @endforeach
                    </einu-table>
                </einu-block>
            </einu-col>
        </einu-grid>
    </einu-section>
@endsection