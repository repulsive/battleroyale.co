<!DOCTYPE html>
<html lang="en">
    <head>

        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <title>BattleRoyale.co</title>

        <!-- Styles -->
        <link href="{{ asset('css/maintenance.css') }}" rel="stylesheet" type="text/css">
    </head>
    <body einu-flow="battleroyale.co">
        <einu-app maintenance>
            <einu-flex-container>
                <einu-block class="content">
                    <img src="{{ asset('img/logo.png') }}" height="300" />
                    <einu-heading size="2">Coming soon!</einu-heading>
                </einu-block>
            </einu-flex-container>
        </einu-app>
    </body>
</html>