<?php

namespace App\Http\Controllers;

use App\Models\Wallet\Address;
use App\Models\Wallet\Transaction;
use Illuminate\Http\Request;

class WalletController extends Controller
{
    public function index()
    {
        $user = auth()->user();

        $address = $user->wallet_address;
        $points = $user->wallet_points;
        $coins = $user->wallet_coins;

        return view('wallet.index', [
            'address' => $address,
            'points' => $points,
            'coins' => $coins,
        ]);
    }

    public function transactions()
    {
        $user = auth()->user();

        $transactions = $user->wallet_transactions()->orderBy('id', 'DESC')->orderBy('created_at', 'DESC')->paginate(25);

        return view('wallet.transactions', [
            'transactions' => $transactions,
        ]);
    }

    public function exchange()
    {
        return view('wallet.exchange');
    }

    public function exchangePost(Request $request)
    {
        $currency = $request->from_currency;
        $amount = $request->from_amount;

        $user = $request->user();

        $fee = $currency == 'BRC' ? $this->__settings->exchange_fee / $this->__settings->exchange_rate : $this->__settings->exchange_fee;
        $total_amount = $amount + $fee;

        $amount_to = $currency == 'BRC' ? $amount * $this->__settings->exchange_rate : $amount / $this->__settings->exchange_rate;

        $table_from = $currency == 'BRC' ? 'wallet_coins' : 'wallet_points';
        $table_to = $currency == 'BRC' ? 'wallet_points' : 'wallet_coins';

        $currency_from = $currency == 'BRC' ? 'BRC' : 'BRP';
        $currency_to = $currency == 'BRC' ? 'BRP' : 'BRC';

        $user->{$table_from}->decrease($total_amount);
        Transaction::create([
            'user_id' => $user->id,
            'type_id' => 8,
            'amount' => $amount,
            'direction' => 0,
            'balance' => $user->{$table_from}->amount,
            'fee' => $fee,
            'currency' => $currency_from,
            'comment' => 'Exchange from <span class="red">'.$currency_from.'</span> to <span class="green">'.$currency_to.'</span>'
        ]);

        $user->{$table_to}->increase($amount_to);
        Transaction::create([
            'user_id' => $user->id,
            'type_id' => 8,
            'amount' => $amount_to,
            'direction' => 1,
            'balance' => $user->{$table_to}->amount,
            'fee' => 0,
            'currency' => $currency_to,
            'comment' => 'Exchange from <span class="red">'.$currency_from.'</span> to <span class="green">'.$currency_to.'</span>'
        ]);

        return redirect()->route('wallet.exchange');
    }

    public function deposit()
    {
        return view('wallet.deposit');
    }

    public function depositPost(Request $request)
    {
    }

    public function send()
    {
        return view('wallet.send');
    }

    public function sendPost(Request $request)
    {
        $currency = $request->currency;
        $amount = $request->amount;

        $address = Address::where('address', $request->address)->first();

        $sender = $request->user();
        $receiver = !is_null($address) ? $address->user : null;

        $fee = $currency == 'BRC' ? $this->__settings->send_fee / $this->__settings->exchange_rate : $this->__settings->send_fee;
        $total_amount = $amount + $fee;

        $table = $currency == 'BRC' ? 'wallet_coins' : 'wallet_points';

        $sender->{$table}->decrease($total_amount);
        Transaction::create([
            'user_id' => $sender->id,
            'type_id' => 3,
            'amount' => $amount,
            'direction' => 0,
            'balance' => $sender->{$table}->amount,
            'fee' => $fee,
            'currency' => $currency,
            'comment' => 'To <span class="yellow console">'.$request->address.'</span>'
        ]);

        if ($receiver !== null) {
            $receiver->{$table}->increase($amount);
            Transaction::create([
                'user_id' => $receiver->id,
                'type_id' => 4,
                'amount' => $amount,
                'direction' => 1,
                'balance' => $receiver->{$table}->amount,
                'fee' => 0,
                'currency' => $currency,
                'comment' => 'From <span class="yellow console">'.$sender->wallet_address->address.'</span>'
            ]);
        }

        return redirect()->route('wallet.send');
    }
}
