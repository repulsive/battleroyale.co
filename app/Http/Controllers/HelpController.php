<?php

namespace App\Http\Controllers;

use App\Models\Misc\Rank;
use App\Models\Misc\Level;
use Illuminate\Http\Request;

class HelpController extends Controller
{
    public function ranks()
    {
        $ranks = Rank::all();

        return view('help.ranks', [
            'ranks' => $ranks
        ]);
    }

    public function levels()
    {
        $levels = Level::all();

        return view('help.levels', [
            'levels' => $levels
        ]);
    }
}
