<?php

namespace App\Http\Controllers;

use App\Models\Setting;
use App\Models\Misc\Role;
use App\Models\Forum\Category;
use App\Models\Forum\Forum;
use App\Models\Forum\Reply;
use App\Models\Forum\Thread;
use App\Models\User;
use Illuminate\Http\Request;

class ForumController extends Controller
{
    public function index()
    {
        $userRole = auth()->user()->roles->first()->level;

        $categories = Category::where('minimum_role', '<=', $userRole)->get();

        $topUser = User::leftJoin('forum_replies','users.id','=','forum_replies.creator_id')
            ->selectRaw('users.*, count(forum_replies.creator_id) AS total')
            ->groupBy('users.id')
            ->orderBy('total','DESC')
            ->first();

        return view('forum.index', [
            'categories' => $categories,
            'topUser' => $topUser
        ]);
    }

    public function forum($slug)
    {
        $id = explode('-', $slug)[0];
        $forum = Forum::where('id', $id)->firstOrFail();
        $threads = $forum->threads()
            ->with('replies')
            ->orderBy('pinned', 'DESC')
            ->orderBy('updated_at', 'DESC')
            ->paginate(25);

        $userRole = auth()->user()->roles->first();

        if ($userRole->level >= $forum->minimum_role) {
            return view('forum.forum', [
                'forum' => $forum,
                'threads' => $threads
            ]);
        } else {
            abort(404);
        }
    }

    public function thread($slug)
    {
        $id = explode('-', $slug)[0];
        $thread = Thread::where('id', $id)->with(['replies'])->firstOrFail();
        $thread->increment('views');

        $replies = $thread->replies()
            ->with(['creator'])
            ->orderBy('created_at', 'ASC')
            ->orderBy('id', 'ASC')
            ->paginate(15);

        $userRole = auth()->user()->roles->first();

        if ($userRole->level >= $thread->forum->minimum_role) {
            return view('forum.thread', [
                'thread' => $thread,
                'replies' => $replies
            ]);
        } else {
            abort(404);
        }
    }

    public function new($slug)
    {
        $id = explode('-', $slug)[0];
        $forum = Forum::where('id', $id)->firstOrFail();

        $userRole = auth()->user()->roles->first();

        if ($userRole->level >= $forum->minimum_role) {
            return view('forum.thread-new', [
                'forum' => $forum
            ]);
        } else {
            abort(404);
        }
    }

    public function store(Request $request, $slug)
    {
        $id = explode('-', $slug)[0];
        $forum = Forum::where('id', $id)->firstOrFail();

        $userRole = auth()->user()->roles->first();

        if ($userRole->level >= $forum->minimum_role) {
            $request->merge(['creator_id' => auth()->user()->id]);
            $thread = new Thread($request->all());
            $reply = new Reply($request->all());

            $forum->threads()->save($thread);
            $thread->replies()->save($reply);

            return redirect()->route('forum.thread', $thread->id);
        } else {
            abort(403);
        }
    }

    public function reply(Request $request, $slug)
    {
        $id = explode('-', $slug)[0];
        $thread = Thread::where('id', $id)->firstOrFail();

        $userRole = auth()->user()->roles->first();

        if ($userRole->level >= $thread->forum->minimum_role) {
            $request->merge(['creator_id' => auth()->user()->id]);
            
            $reply = new Reply($request->all());

            $thread->replies()->save($reply);

            return redirect()->route('forum.thread', $thread->id);
        } else {
            abort(403);
        }
    }

    public function destroy(Request $request, $slug)
    {
        $id = explode('-', $slug)[0];
        $junkyard = $this->__settings->forum_junkyard_id;
        $thread = Thread::where('id', $id)->first();

        if ($junkyard != null) {
            $thread->forum_id = $junkyard->value;
            $thread->locked = true;
            $thread->save();

            return redirect()->route('forum.forum', $junkyard->value);
        } else {
            $thread->delete();

            return redirect()->route('forum.forum', $thread->forum->id);
        }
    }

    public function lock($slug)
    {
        $id = explode('-', $slug)[0];
        $thread = Thread::where('id', $id)->first();
        $thread->locked = $thread->locked ? false : true;
        $thread->save();

        return redirect()->route('forum.thread', $id);
    }

    public function pin($slug)
    {
        $id = explode('-', $slug)[0];
        $thread = Thread::where('id', $id)->first();
        $thread->pinned = $thread->pinned ? false : true;
        $thread->save();

        return redirect()->route('forum.thread', $id);
    }

    public function feature($slug)
    {
        $id = explode('-', $slug)[0];
        $thread = Thread::where('id', $id)->first();
        $thread->featured = $thread->featured ? false : true;
        $thread->save();

        return redirect()->route('forum.thread', $id);
    }
}
