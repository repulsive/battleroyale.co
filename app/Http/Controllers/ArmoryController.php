<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ArmoryController extends Controller
{
    public function index()
    {
        return json_encode($this->__settings);
    }
}
