<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ExperienceController extends Controller
{
    public function index()
    {
        $user = auth()->user();

        $rank = $user->level->rank;
        $level = $user->level;

        return view('experience.index', [
            'rank' => $rank,
            'level' => $level
        ]);
    }
}
