<?php

namespace App\Http\Middleware;

use App\Models\Misc\Role;
use Closure;

class CheckRole
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string  $role
     * @return mixed
     */
    public function handle($request, Closure $next, $role)
    {
        if (auth()->check()) {
            $neededRole = Role::where('name', $role)->firstOrFail();
            $usersRole = auth()->user()->roles()->orderBy('level', 'desc')->first();

            if ($usersRole->level >= $neededRole->level) {
                return $next($request);
            }

            abort(403);
        }
        return $next($request);
    }
}
