<?php

namespace App\Traits;

use App\Models\Wallet\Address;
use App\Models\Wallet\Points;
use App\Models\Wallet\Coins;
use App\Models\Wallet\Transaction;
use Webpatser\Uuid\Uuid;

trait HasWallet
{
    public static function bootHasWallet()
    {
        static::created(function ($model) {
            Points::create(['user_id' => $model->id]);
            Coins::create(['user_id' => $model->id]);
            Address::create(['user_id' => $model->id, 'address' => Uuid::generate()->string]);
        });
    }
    
    public function wallet_coins()
    {
        return $this->hasOne(Coins::class);
    }

    public function wallet_points()
    {
        return $this->hasOne(Points::class);
    }

    public function wallet_address()
    {
        return $this->hasOne(Address::class);
    }

    public function wallet_transactions()
    {
        return $this->hasMany(Transaction::class);
    }
}