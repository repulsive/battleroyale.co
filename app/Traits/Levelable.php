<?php

namespace App\Traits;

use Illuminate\Support\Facades\Storage;
use App\Models\Misc\Experience;
use App\Models\Misc\Level;
use App\Models\Misc\Role;

trait Levelable
{
    protected $level_instance;

    public static function bootLevelable()
    {
        static::created(function ($model) {
            Experience::create(['user_id' => $model->id]);
            $model->assignRole(1);
        });
    }

    /**
     * User's experience.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function experience()
    {
        return $this->hasOne(Experience::class);
    }

    /**
     * User's roles.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function roles()
    {
        return $this->belongsToMany(Role::class)->orderBy('level', 'DESC')->withTimestamps();
    }

    /**
     * User's level.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function getLevelAttribute()
    {
        if ($this->level_instance != null)
            return $this->level_instance;
        else
            return $this->level_instance = Level::get($this->experience->points);
    }

    /**
     * Check if user has a role
     *
     * @param $name
     * @return bool
     */
    public function hasRole($name)
    {
        foreach ($this->roles as $role)
        {
            if (strtolower($role->name) == strtolower($name)) return true;
        }

        return false;
    }

    /**
     * Assign a role to the user
     *
     * @param $role
     * @return mixed
     */
    public function assignRole($role)
    {
        if (!$this->hasRole($role))
            return $this->roles()->attach($role);
    }

    /**
     * Revoke a role from the user
     *
     * @param $role
     * @return mixed
     */
    public function revokeRole($role)
    {
        if (!$this->hasRole($role))
            return $this->roles()->detach($role);
    }

    /**
     * Base64 representation of user's badge.
     *
     * @return string
     */
    public function getBadgeAttribute()
    {
        $role_badge = $this->roles()->orderBy('level', 'desc')->first()->badge_path;
        if ($role_badge == null)
            $image = Storage::get($this->rank->badge_path);
        else
            $image = Storage::get($role_badge);

        return $image_base64 = 'data:image/svg+xml;base64,'.base64_encode($image);
    }
}