<?php

namespace App\Models\Forum;

use Illuminate\Database\Eloquent\Model;

class Forum extends Model
{
    protected $table = 'forum_forums';
    protected $fillable = ['category_id', 'title', 'disabled'];
    protected $with = ['threads'];

    public function threads()
    {
        return $this->hasMany(Thread::class)
            ->orderBy('pinned', 'DESC')
            ->orderBy('updated_at', 'DESC');
    }
    
    public function category()
    {
        return $this->belongsTo(Category::class);
    }

    public function getThreadCountAttribute()
    {
        return $this->threads->count();
    }
}
