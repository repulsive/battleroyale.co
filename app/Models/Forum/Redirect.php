<?php

namespace App\Models\Forum;

use Illuminate\Database\Eloquent\Model;

class Redirect extends Model
{
    protected $table = 'forum_redirects';
    protected $fillable = ['category_id', 'title', 'link'];

    public function category()
    {
        return $this->belongsTo(Category::class);
    }
}
