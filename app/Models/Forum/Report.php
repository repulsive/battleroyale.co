<?php

namespace App\Models\Forum;

use App\Models\User;
use Illuminate\Database\Eloquent\Model;

class Report extends Model
{
    protected $fillable = ['user_id', 'reply_id'];
    protected $table = 'forum_reports';

    public function user()
    {
        return $this->hasOne(User::class);
    }

    public function reply()
    {
        return $this->hasOne(Reply::class);
    }
}
