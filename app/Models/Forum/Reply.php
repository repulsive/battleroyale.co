<?php

namespace App\Models\Forum;

use App\Models\User;
use Illuminate\Database\Eloquent\Model;

class Reply extends Model
{
    protected $table = 'forum_replies';
    protected $fillable = ['thread_id', 'creator_id', 'content'];
    protected $touches = ['thread'];
    protected $with = ['creator'];
    
    public function thread()
    {
        return $this->belongsTo(Thread::class);
    }

    public function reports()
    {
        return $this->hasMany(Report::class);
    }

    public function creator()
    {
        return $this->belongsTo(User::class);
    }
}
