<?php

namespace App\Models\Forum;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $table = 'forum_categories';
    protected $fillable = ['title', 'full_width', 'minimum_role'];
    protected $with = ['forums', 'redirects'];
    
    public function forums()
    {
        return $this->hasMany(Forum::class);
    }

    public function redirects()
    {
        return $this->hasMany(Redirect::class);
    }
}

/*
background: url(http://i.imgur.com/GM8QjV0.gif);
    color: #e5d1ff;
    font-weight: bold;
    text-shadow: 0 0 0.9em #ffffff;
*/