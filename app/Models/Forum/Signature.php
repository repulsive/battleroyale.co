<?php

namespace App\Models\Forum;

use App\Models\User;
use Illuminate\Database\Eloquent\Model;

class Signature extends Model
{
    protected $fillable = ['content'];
    protected $table = 'forum_signatures';
    
    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
