<?php

namespace App\Models\Forum;

use App\Models\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Thread extends Model
{
    use SoftDeletes;

    protected $table = 'forum_threads';
    protected $fillable = ['forum_id', 'creator_id', 'title', 'locked', 'pinned', 'featured', 'views'];
    protected $dates = ['deleted_at'];
    protected $with = ['replies', 'creator'];

    public function forum()
    {
        return $this->belongsTo(Forum::class);
    }

    public function replies()
    {
        return $this->hasMany(Reply::class)
            ->orderBy('created_at', 'ASC')
            ->orderBy('id', 'ASC');
    }

    public function creator()
    {
        return $this->belongsTo(User::class);
    }

    public function getReplyCountAttribute()
    {
        return $this->replies->count();
    }

    public function getLastReplyPageAttribute()
    {
        $repliesPerPage = 15;
        $pageNumber = (int) ($this->reply_count / $repliesPerPage + 1);
        return $pageNumber;
    }
}
