<?php

namespace App\Models\News;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $table = 'news_categories';
    
    public function posts()
    {
        return $this->hasMany(Post::class);
    }
}
