<?php

namespace App\Models\Wallet;

use Illuminate\Database\Eloquent\Model;

class TransactionType extends Model
{
    protected $table = 'wallet_transaction_types';
    protected $fillable = ['name', 'icon_code'];

    public function transactions()
    {
        return $this->hasMany(Transaction::class);
    }
}
