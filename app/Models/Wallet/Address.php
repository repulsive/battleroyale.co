<?php

namespace App\Models\Wallet;

use App\Models\User;
use Illuminate\Database\Eloquent\Model;

class Address extends Model
{
    protected $fillable = ['user_id', 'address'];
    protected $table = 'wallet_addresses';
    
    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
