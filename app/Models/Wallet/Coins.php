<?php

namespace App\Models\Wallet;

use Illuminate\Database\Eloquent\Model;

class Coins extends Model
{
    protected $table = 'wallet_coins';
    protected $fillable = ['user_id'];
    protected $hidden = ['created_at', 'updated_at', 'user_id', 'id'];

    public function increase($amount)
    {
        $this->increment('amount', $amount);

        return $this;
    }

    public function decrease($amount)
    {
        $this->decrement('amount', $amount);

        return $this;
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
