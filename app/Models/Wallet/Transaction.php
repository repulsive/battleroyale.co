<?php

namespace App\Models\Wallet;

use App\Models\User;
use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{
    protected $table = 'wallet_transactions';
    public $fillable = ['user_id', 'type_id', 'amount', 'direction', 'balance', 'fee', 'currency', 'comment'];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function type()
    {
        return $this->belongsTo(TransactionType::class, 'type_id');
    }
}
