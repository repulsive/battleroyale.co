<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Setting extends Model
{
    protected $fillable = ['key', 'value'];

    public static function getAll()
    {
        $settings = self::all();
        $_settings = new \StdClass();

        foreach($settings as $setting) {
            $_settings->{$setting->key} = $setting->value;
        }

        return $_settings;
    }

    public static function get($key)
    {
        return self::where('key', $key)->find()->value;
    }
}
