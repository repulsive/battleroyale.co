<?php

namespace App\Models;

use App\Models\Forum\Report;
use App\Models\Forum\Signature;
use App\Models\Forum\Thread;
use App\Models\Forum\Reply;

use App\Models\News\Post;
use App\Models\News\Comment;

use App\Traits\HasWallet;
use App\Traits\Levelable;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable, HasWallet, Levelable;

    protected $fillable = ['name', 'email', 'password'];
    protected $hidden = ['password', 'remember_token'];
    protected $with = ['roles'];
    
    public function forum_threads()
    {
        return $this->hasMany(Thread::class, 'creator_id');
    }

    public function forum_replies()
    {
        return $this->hasMany(Reply::class, 'creator_id');
    }

    public function forum_reports()
    {
        return $this->hasMany(Report::class);
    }

    public function forum_signature()
    {
        return $this->hasOne(Signature::class);
    }

    public function news_posts()
    {
        return $this->hasMany(Post::class, 'creator_id');
    }

    public function news_comments()
    {
        return $this->hasMany(Comment::class, 'creator_id');
    }
}
