<?php

namespace App\Models\Misc;

use App\Models\User;
use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    protected $table = 'roles';
    protected $fillable = ['name', 'level', 'badge_path'];
    protected $hidden = ['created_at', 'updated_at', 'pivot', 'badge_path', 'id'];
    
    public function users()
    {
        $this->hasMany(User::class);
    }
}
