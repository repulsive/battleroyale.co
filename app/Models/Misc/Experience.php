<?php

namespace App\Models\Misc;

use Illuminate\Database\Eloquent\Model;

class Experience extends Model
{
    protected $table = 'experience';
    protected $fillable = ['user_id'];
    protected $hidden = ['created_at', 'updated_at', 'user_id', 'id'];

    /**
     * Increase user's experience
     *
     * @param $points
     * @return $this
     */
    public function increase($points)
    {
        $this->increment('points', $points);

        return $this;
    }

    /**
     * Decrease user's experience
     *
     * @param $points
     * @return $this
     */
    public function decrease($points)
    {
        $this->decrement('points', $points);

        return $this;
    }
}
