<?php

namespace App\Models\Misc;

use Illuminate\Database\Eloquent\Model;

class Rank extends Model
{
    protected $table = 'ranks';
    protected $fillable = ['name', 'badge_path', 'background_path', 'color'];
    protected $hidden = ['created_at', 'updated_at', 'badge_path', 'id'];
    protected $with = ['levels'];
    
    public function levels()
    {
        return $this->hasMany(Level::class);
    }

    public function getRequiredLevelAttribute()
    {
        $levels = $this->levels;
        $requiredLevel = 0;
        foreach ($levels as $level) {
            $requiredLevel = $level;

            if ($level->level < $requiredLevel->level) {
                break;
            }
        }

        return $requiredLevel;
    }
}
