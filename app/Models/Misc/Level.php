<?php

namespace App\Models\Misc;

use Illuminate\Database\Eloquent\Model;

class Level extends Model
{
    protected $table = 'levels';
    protected $fillable = ['rank_id', 'level'];
    protected $hidden = ['created_at', 'updated_at', 'id'];

    protected static $max_level = 100;

    public function rank()
    {
        return $this->belongsTo(Rank::class);
    }

    /**
     * Constant that decides how much
     * experience you need to get level up
     *
     * @var int
     */
    private static $base = 150;

    /**
     * Constant that decides how much
     * daily points you receive
     *
     * @var int
     */
    private static $daily_base = 10;

    /**
     * Constant that decides the increase
     * of experience you need for each level
     *
     * @var float
     */
    private static $factor = 1.22;

    /**
     * Constant that decides the increase
     * of daily points you receive
     *
     * @var float
     */
    private static $daily_factor = 1.08;
    private static $daily_factor2 = 1.09;

    /**
     * Gets a level based on experience
     *
     * @param $experience
     * @return mixed
     */
    public static function get($experience)
    {
        $levels = self::levels();
        $max_level = self::max_level();

        foreach ($levels as $level => $xp) {
            if (array_key_exists($level + 1, $levels)) {
                if (intval($experience) < $levels[$level + 1]) {
                    $lvl = $level;
                    break;
                }
            } else {
                $lvl = $max_level;
                break;
            }
        }


        return self::where('level', $lvl)->firstOrFail();
    }

    /**
     * Returns required experience for that level
     *
     * @return mixed
     */
    public function getRequiredExperienceAttribute()
    {
        return static::levels()[$this->level];
    }

    /**
     * Returns daily points you receive on current level
     *
     * @return mixed
     */
    public function getDailyPointsAttribute()
    {
        return static::dailyPoints()[$this->level];
    }

    /**
     * Returns maximum level that a user can be
     *
     * @return mixed
     */
    private static function max_level()
    {
        return static::$max_level;
    }

    /**
     * Calculates experience required to get a certain level
     *
     * @return array
     */
    private static function levels()
    {
        $levels = [];
        $max_level = self::max_level();

        $levels[1] = 0;

        for ($level = 1; $level <= $max_level-1; $level++) {
            $levels[$level + 1] = round(self::$base * pow(self::$factor, $level + 1));
        }

        return $levels;
    }

    private static function dailyPoints()
    {
        $levels = [];
        $max_level = self::max_level();

        $levels[1] = 10;

        for ($level = 1; $level <= $max_level-1; $level++) {
            if ($level <= 90) {
                $levels[$level + 1] = round(self::$daily_base * pow(self::$daily_factor, $level + 1));
            } else {
                $levels[$level + 1] = round(self::$daily_base * pow(self::$daily_factor2, $level + 1));
            }
        }

        return $levels;
    }
}
