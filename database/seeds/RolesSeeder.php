<?php

use Illuminate\Database\Seeder;

class RolesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('roles')->delete();

        $roles = array(
            array('name' => 'Member', 'level' => 1, 'badge_path' => '', 'color' => ''),
            array('name' => 'Donator', 'level' => 2, 'badge_path' => '', 'color' => ''),
            array('name' => 'VIP', 'level' => 3, 'badge_path' => '', 'color' => ''),
            array('name' => 'Moderator', 'level' => 4, 'badge_path' => '', 'color' => ''),
            array('name' => 'Administrator', 'level' => 5, 'badge_path' => '', 'color' => ''),
            array('name' => 'Owner', 'level' => 6, 'badge_path' => '', 'color' => ''),
        );

        DB::table('roles')->insert($roles);
    }
}
