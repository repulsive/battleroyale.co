<?php

use Illuminate\Database\Seeder;

class TransactionTypesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        DB::table('wallet_transaction_types')->delete();

        $transaction_types = array(
            array('name' => 'Deposit', 'icon_code' => 'arrow-circle-down'),
            array('name' => 'Withdraw', 'icon_code' => 'arrow-circle-up'),
            array('name' => 'Sent', 'icon_code' => 'circle-o'),
            array('name' => 'Received', 'icon_code' => 'circle'),
            array('name' => 'Subscription', 'icon_code' => 'refresh'),
            array('name' => 'Donation', 'icon_code' => 'plus-circle'),
            array('name' => 'Market', 'icon_code' => 'minus-circle'),
            array('name' => 'Exchange', 'icon_code' => 'exchange'),
        );

        DB::table('wallet_transaction_types')->insert($transaction_types);
    }
}
