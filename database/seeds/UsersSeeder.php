<?php

use Illuminate\Database\Seeder;

class UsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
//        DB::table('users')->delete();
//
//        $users = array(
//            array('name' => 'Member', 'level' => 1, 'badge_path' => '', 'color' => ''),
//            array('name' => 'VIP', 'level' => 2, 'badge_path' => '', 'color' => ''),
//            array('name' => 'Moderator', 'level' => 3, 'badge_path' => '', 'color' => ''),
//            array('name' => 'Administrator', 'level' => 4, 'badge_path' => '', 'color' => ''),
//            array('name' => 'Owner', 'level' => 5, 'badge_path' => '', 'color' => ''),
//        );
//
//        DB::table('users')->insert($roles);

        factory(\App\Models\User::class, 50)->create();
    }
}
