<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(SettingsSeeder::class);
        $this->call(RolesSeeder::class);
        $this->call(RanksSeeder::class);
        $this->call(LevelsSeeder::class);
        $this->call(UsersSeeder::class);
        $this->call(ForumCategoriesSeeder::class);
        $this->call(ForumForumsSeeder::class);
        $this->call(ForumThreadsSeeder::class);
        $this->call(TransactionTypesSeeder::class);
        //$this->call(TransactionsSeeder::class);
    }
}
