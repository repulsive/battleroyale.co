<?php

use Illuminate\Database\Seeder;

class RanksSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('ranks')->delete();

        $ranks = array(
            array('name' => 'Pilgrim', 'badge_path' => 'img/badges/badge-1_1.png', 'background_path' => 'img/badges/badge-1-background.png', 'color' => '#8e8e8e'),
            array('name' => 'Baron', 'badge_path' => 'img/badges/badge-1_2.png', 'background_path' => 'img/badges/badge-1-background.png', 'color' => '#8e8e8e'),
            array('name' => 'Professor', 'badge_path' => 'img/badges/badge-1_3.png', 'background_path' => 'img/badges/badge-1-background.png', 'color' => '#8e8e8e'),
            array('name' => 'Ambassador', 'badge_path' => 'img/badges/badge-2_1.png', 'background_path' => 'img/badges/badge-2-background.png', 'color' => '#4193a3'),
            array('name' => 'Governer', 'badge_path' => 'img/badges/badge-2_2.png', 'background_path' => 'img/badges/badge-2-background.png', 'color' => '#4193a3'),
            array('name' => 'Executor', 'badge_path' => 'img/badges/badge-2_3.png', 'background_path' => 'img/badges/badge-2-background.png', 'color' => '#4193a3'),
            array('name' => 'Commander', 'badge_path' => 'img/badges/badge-3_1.png', 'background_path' => 'img/badges/badge-3-background.png', 'color' => '#3ba053'),
            array('name' => 'Emperor', 'badge_path' => 'img/badges/badge-3_2.png', 'background_path' => 'img/badges/badge-3-background.png', 'color' => '#3ba053'),
            array('name' => 'General', 'badge_path' => 'img/badges/badge-3_3.png', 'background_path' => 'img/badges/badge-3-background.png', 'color' => '#3ba053'),
            array('name' => 'Sentinel', 'badge_path' => 'img/badges/badge-4.png', 'background_path' => 'img/badges/badge-4-background.png', 'color' => '#db4658'),
        );

        DB::table('ranks')->insert($ranks);
    }
}
