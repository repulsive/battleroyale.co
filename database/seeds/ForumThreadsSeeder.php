<?php

use Illuminate\Database\Seeder;

class ForumThreadsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(\App\Models\Forum\Thread::class, rand(15, 125))
            ->create(['creator_id' => rand(1, 50), 'forum_id' => 1])
            ->each(function ($thread) {
                $thread->replies()->saveMany(factory(\App\Models\Forum\Reply::class, 1)->make(['creator_id' => $thread->creator_id]));
                $thread->replies()->saveMany(factory(\App\Models\Forum\Reply::class, rand(15, 125))->make(['creator_id' => rand(1, 50)]));
        });
    }
}
