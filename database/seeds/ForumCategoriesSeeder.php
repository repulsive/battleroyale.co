<?php

use Illuminate\Database\Seeder;

class ForumCategoriesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('forum_categories')->delete();

        $categories = array(
            array('title' => 'BattleRoyale.co VIP Area', 'full_width' => true, 'minimum_role' => '3'), // 1
            array('title' => 'BattleRoyale.co Links', 'full_width' => false, 'minimum_role' => '1'), // 2
            array('title' => 'General and News', 'full_width' => false, 'minimum_role' => '1'), // 3
            array('title' => 'Questions and Answers', 'full_width' => false, 'minimum_role' => '1'), // 4
            array('title' => 'PUBG', 'full_width' => false, 'minimum_role' => '1'), // 5
            array('title' => 'H1Z1', 'full_width' => false, 'minimum_role' => '1'), // 6
            array('title' => 'Fortnite', 'full_width' => false, 'minimum_role' => '1'), // 7
            array('title' => 'Other BattleRoyale Titles', 'full_width' => false, 'minimum_role' => '1'), // 8
            array('title' => 'Off-topic', 'full_width' => false, 'minimum_role' => '1'), // 9
        );

        DB::table('forum_categories')->insert($categories);
    }
}
