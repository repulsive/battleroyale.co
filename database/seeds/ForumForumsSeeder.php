<?php

use Illuminate\Database\Seeder;

class ForumForumsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('forum_forums')->delete();

        $forums = array(
            array('title' => 'VIP Main (General Discussion)', 'category_id' => 1, 'disabled' => false, 'minimum_role' => '3'),
            array('title' => 'VIP Releases and Sharing', 'category_id' => 1, 'disabled' => false, 'minimum_role' => '3'),

            array('title' => 'Main', 'category_id' => 3, 'disabled' => false, 'minimum_role' => '1'),
            array('title' => 'F.A.Q.', 'category_id' => 3, 'disabled' => false, 'minimum_role' => '1'),
            array('title' => 'Ranks', 'category_id' => 3, 'disabled' => false, 'minimum_role' => '1'),
            array('title' => 'How to become VIP?', 'category_id' => 3, 'disabled' => false, 'minimum_role' => '1'),
            array('title' => 'Rules and Forum Guides', 'category_id' => 3, 'disabled' => false, 'minimum_role' => '1'),
            array('title' => 'News and Announcements', 'category_id' => 3, 'disabled' => false, 'minimum_role' => '1'),
            array('title' => 'Forum Suggestions', 'category_id' => 3, 'disabled' => false, 'minimum_role' => '1'),
            array('title' => 'Videos and Streams', 'category_id' => 3, 'disabled' => false, 'minimum_role' => '1'),
            array('title' => 'Introduce yourself', 'category_id' => 3, 'disabled' => false, 'minimum_role' => '1'),
            array('title' => 'Complaint Area', 'category_id' => 3, 'disabled' => false, 'minimum_role' => '1'),
            array('title' => 'Support Area', 'category_id' => 3, 'disabled' => false, 'minimum_role' => '1'),

            array('title' => 'Questions and Answers', 'category_id' => 4, 'disabled' => false, 'minimum_role' => '1'),

            array('title' => 'General Discussions / Questions', 'category_id' => 5, 'disabled' => false, 'minimum_role' => '1'),
            array('title' => 'Guides and Tactics', 'category_id' => 5, 'disabled' => false, 'minimum_role' => '1'),
            array('title' => 'News and upcoming updates', 'category_id' => 5, 'disabled' => false, 'minimum_role' => '1'),
            array('title' => 'Funny / EPIC Videos (Streams)', 'category_id' => 5, 'disabled' => false, 'minimum_role' => '1'),
            array('title' => 'Promote your content', 'category_id' => 5, 'disabled' => false, 'minimum_role' => '1'),
            array('title' => 'Looking for Team', 'category_id' => 5, 'disabled' => false, 'minimum_role' => '1'),

            array('title' => 'General Discussions / Questions', 'category_id' => 6, 'disabled' => false, 'minimum_role' => '1'),
            array('title' => 'Guides and Tactics', 'category_id' => 6, 'disabled' => false, 'minimum_role' => '1'),
            array('title' => 'News and upcoming updates', 'category_id' => 6, 'disabled' => false, 'minimum_role' => '1'),
            array('title' => 'Funny / EPIC Videos (Streams)', 'category_id' => 6, 'disabled' => false, 'minimum_role' => '1'),
            array('title' => 'Promote your content', 'category_id' => 6, 'disabled' => false, 'minimum_role' => '1'),
            array('title' => 'Looking for Team', 'category_id' => 6, 'disabled' => false, 'minimum_role' => '1'),

            array('title' => 'General Discussions / Questions', 'category_id' => 7, 'disabled' => false, 'minimum_role' => '1'),
            array('title' => 'Guides and Tactics', 'category_id' => 7, 'disabled' => false, 'minimum_role' => '1'),
            array('title' => 'News and upcoming updates', 'category_id' => 7, 'disabled' => false, 'minimum_role' => '1'),
            array('title' => 'Funny / EPIC Videos (Streams)', 'category_id' => 7, 'disabled' => false, 'minimum_role' => '1'),
            array('title' => 'Promote your content', 'category_id' => 7, 'disabled' => false, 'minimum_role' => '1'),
            array('title' => 'Looking for Team', 'category_id' => 7, 'disabled' => false, 'minimum_role' => '1'),

            array('title' => 'General Discussions / Questions', 'category_id' => 8, 'disabled' => false, 'minimum_role' => '1'),
            array('title' => 'Guides and Tactics', 'category_id' => 8, 'disabled' => false, 'minimum_role' => '1'),
            array('title' => 'News and upcoming updates', 'category_id' => 8, 'disabled' => false, 'minimum_role' => '1'),
            array('title' => 'Funny / EPIC Videos (Streams)', 'category_id' => 8, 'disabled' => false, 'minimum_role' => '1'),
            array('title' => 'Promote your content', 'category_id' => 8, 'disabled' => false, 'minimum_role' => '1'),
            array('title' => 'Looking for Team', 'category_id' => 8, 'disabled' => false, 'minimum_role' => '1'),

            array('title' => 'Off-topic', 'category_id' => 9, 'disabled' => false, 'minimum_role' => '1'),
            array('title' => 'Junkyard', 'category_id' => 9, 'disabled' => true, 'minimum_role' => '1'),
        );

        DB::table('forum_forums')->insert($forums);
    }
}
