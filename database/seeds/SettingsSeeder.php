<?php

use Illuminate\Database\Seeder;

class SettingsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('settings')->delete();

        $settings = array(
            //array('key' => '', 'value' => ''),
            array('key' => 'exchange_rate', 'value' => '1000'),
            array('key' => 'exchange_fee', 'value' => '200'),
            array('key' => 'send_fee', 'value' => '200'),
            array('key' => 'forum_junkyard_id', 'value' => '40'),
        );

        DB::table('settings')->insert($settings);
    }
}
