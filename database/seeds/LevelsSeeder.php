<?php

use Illuminate\Database\Seeder;

class LevelsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('levels')->delete();

        $levels = array(
            array('rank_id' => 1, 'level' => 1),
            array('rank_id' => 1, 'level' => 2),
            array('rank_id' => 1, 'level' => 3),
            array('rank_id' => 1, 'level' => 4),
            array('rank_id' => 1, 'level' => 5),
            array('rank_id' => 1, 'level' => 6),
            array('rank_id' => 1, 'level' => 7),
            array('rank_id' => 1, 'level' => 8),
            array('rank_id' => 1, 'level' => 9),
            array('rank_id' => 1, 'level' => 10),
            array('rank_id' => 1, 'level' => 11),
            array('rank_id' => 1, 'level' => 12),
            array('rank_id' => 1, 'level' => 13),
            array('rank_id' => 1, 'level' => 14),

            array('rank_id' => 2, 'level' => 15),
            array('rank_id' => 2, 'level' => 16),
            array('rank_id' => 2, 'level' => 17),
            array('rank_id' => 2, 'level' => 18),
            array('rank_id' => 2, 'level' => 19),
            array('rank_id' => 2, 'level' => 20),
            array('rank_id' => 2, 'level' => 21),
            array('rank_id' => 2, 'level' => 22),
            array('rank_id' => 2, 'level' => 23),
            array('rank_id' => 2, 'level' => 24),
            array('rank_id' => 2, 'level' => 25),
            array('rank_id' => 2, 'level' => 26),
            array('rank_id' => 2, 'level' => 27),
            array('rank_id' => 2, 'level' => 28),
            array('rank_id' => 2, 'level' => 29),

            array('rank_id' => 3, 'level' => 30),
            array('rank_id' => 3, 'level' => 31),
            array('rank_id' => 3, 'level' => 32),
            array('rank_id' => 3, 'level' => 33),
            array('rank_id' => 3, 'level' => 34),
            array('rank_id' => 3, 'level' => 35),
            array('rank_id' => 3, 'level' => 36),
            array('rank_id' => 3, 'level' => 37),
            array('rank_id' => 3, 'level' => 38),
            array('rank_id' => 3, 'level' => 39),
            array('rank_id' => 3, 'level' => 40),
            array('rank_id' => 3, 'level' => 41),
            array('rank_id' => 3, 'level' => 42),
            array('rank_id' => 3, 'level' => 43),
            array('rank_id' => 3, 'level' => 44),

            array('rank_id' => 4, 'level' => 45),
            array('rank_id' => 4, 'level' => 46),
            array('rank_id' => 4, 'level' => 47),
            array('rank_id' => 4, 'level' => 48),
            array('rank_id' => 4, 'level' => 49),
            array('rank_id' => 4, 'level' => 50),
            array('rank_id' => 4, 'level' => 51),
            array('rank_id' => 4, 'level' => 52),
            array('rank_id' => 4, 'level' => 53),
            array('rank_id' => 4, 'level' => 54),
            array('rank_id' => 4, 'level' => 55),
            array('rank_id' => 4, 'level' => 56),
            array('rank_id' => 4, 'level' => 57),
            array('rank_id' => 4, 'level' => 58),
            array('rank_id' => 4, 'level' => 59),

            array('rank_id' => 5, 'level' => 60),
            array('rank_id' => 5, 'level' => 61),
            array('rank_id' => 5, 'level' => 62),
            array('rank_id' => 5, 'level' => 63),
            array('rank_id' => 5, 'level' => 64),
            array('rank_id' => 5, 'level' => 65),
            array('rank_id' => 5, 'level' => 66),
            array('rank_id' => 5, 'level' => 67),
            array('rank_id' => 5, 'level' => 68),
            array('rank_id' => 5, 'level' => 69),
            array('rank_id' => 5, 'level' => 70),
            array('rank_id' => 5, 'level' => 71),
            array('rank_id' => 5, 'level' => 72),
            array('rank_id' => 5, 'level' => 73),
            array('rank_id' => 5, 'level' => 74),

            array('rank_id' => 6, 'level' => 75),
            array('rank_id' => 6, 'level' => 76),
            array('rank_id' => 6, 'level' => 77),
            array('rank_id' => 6, 'level' => 78),
            array('rank_id' => 6, 'level' => 79),
            array('rank_id' => 6, 'level' => 80),
            array('rank_id' => 6, 'level' => 81),
            array('rank_id' => 6, 'level' => 82),
            array('rank_id' => 6, 'level' => 83),
            array('rank_id' => 6, 'level' => 84),
            array('rank_id' => 6, 'level' => 85),
            array('rank_id' => 6, 'level' => 86),
            array('rank_id' => 6, 'level' => 87),
            array('rank_id' => 6, 'level' => 88),
            array('rank_id' => 6, 'level' => 89),

            array('rank_id' => 7, 'level' => 90),
            array('rank_id' => 7, 'level' => 91),
            array('rank_id' => 7, 'level' => 92),
            array('rank_id' => 7, 'level' => 93),
            array('rank_id' => 7, 'level' => 94),

            array('rank_id' => 8, 'level' => 95),
            array('rank_id' => 8, 'level' => 96),
            array('rank_id' => 8, 'level' => 97),

            array('rank_id' => 9, 'level' => 98),
            array('rank_id' => 9, 'level' => 99),

            array('rank_id' => 10, 'level' => 100),
        );

        DB::table('levels')->insert($levels);
    }
}
