<?php

use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(App\Models\Wallet\Transaction::class, function (Faker $faker) {
    $base_balance = $faker->randomFloat(4, 41748, 7589059);
    $amount = $faker->randomFloat(4, 10, 41747);
    $fee = $faker->randomFloat(4, 0, 10);
    $type_id = rand(1, 7);
    $currency = rand(0, 1);
    $direction = ($type_id == 1 || $type_id == 4) ? 1 : 0;

    return [
        'type_id' => $type_id,
        'amount' => $amount,
        'direction' => $direction,
        'balance' => $direction ? ($base_balance + $amount) - $fee : ($base_balance - $amount) - $fee,
        'fee' => $fee,
        'currency' => $currency ? 'BRP' : 'BRC',
        'comment' => $faker->text(50),
        'created_at' => $faker->dateTime(),
    ];
});
