<?php

use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(App\Models\Forum\Thread::class, function (Faker $faker) {
    return [
        'title' => $faker->sentence,
        'locked' => rand(0, 1),
        'pinned' => rand(0, 1),
        'featured' => rand(0, 1),
        'views' => rand(123, 12345)
    ];
});
