<?php

// -----------------------------------------------------------------------------------------
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Cookie;

Route::get('/dev-protect', function() {
    \Debugbar::disable();
    return view('dev-protect');
})->name('dev-protect');

Route::post('/dev-protect', function() {
    $_username = 'einutech';
    $_password = 'ekoset14';

    $username = Request::input('username');
    $password = Request::input('password');

    if (($username == $_username) && ($password == $_password)) {
        Cookie::queue(Cookie::make('temp_login', true, 3600));
        return redirect('/');
    } else {
        echo "Wrong username or password";
    }
});
// -----------------------------------------------------------------------------------------

Route::get('/repulsive', function() {
    return view('repulsive');
});

Route::middleware(['dev.protect'])->group(function () {
    Route::get('/login', function() {
        Auth::loginUsingId(1, true);
        return redirect()->route('forum.index');
    });

    Route::get('/logout', 'Auth\LoginController@logout')->name('auth.logout');

    Route::get('/', 'NewsController@index')->name('news.index');

    Route::get('/profile/{username}', 'ProfileController@show')->name('profile');

    Route::get('/forum', 'ForumController@index')->name('forum.index');
    Route::get('/forum/{slug}', 'ForumController@forum')->name('forum.forum');
    Route::get('/forum/{slug}/new', 'ForumController@new')->name('forum.new');
    Route::post('/forum/{slug}/new', 'ForumController@store');
    Route::get('/forum/thread/{slug}', 'ForumController@thread')->name('forum.thread');
    Route::post('/forum/thread/{slug}', 'ForumController@reply');
    Route::delete('/forum/thread/{slug}', 'ForumController@destroy');
    Route::get('/forum/thread/{slug}/lock', 'ForumController@lock')->name('forum.thread.lock');
    Route::get('/forum/thread/{slug}/pin', 'ForumController@pin')->name('forum.thread.pin');
    Route::get('/forum/thread/{slug}/feature', 'ForumController@feature')->name('forum.thread.feature');
    Route::get('/forum/thread/{slug}/edit', 'ForumController@edit')->name('forum.edit');
    Route::patch('/forum/thread/{slug}/edit', 'ForumController@update');
    Route::delete('/forum/replies/{slug}', 'ForumController@destroyReply')->name('forum.destroyReply');

    Route::get('/wallet', 'WalletController@index')->name('wallet.index');
    Route::get('/wallet/transactions', 'WalletController@transactions')->name('wallet.transactions');
    Route::get('/wallet/еxchange', 'WalletController@exchange')->name('wallet.exchange');
    Route::post('/wallet/еxchange', 'WalletController@exchangePost');
    Route::get('/wallet/deposit', 'WalletController@deposit')->name('wallet.deposit');
    Route::post('/wallet/deposit', 'WalletController@depositPost');
    Route::get('/wallet/send', 'WalletController@send')->name('wallet.send');
    Route::post('/wallet/send', 'WalletController@sendPost');

    Route::get('/help/support', 'HelpController@support')->name('help.support');
    Route::get('/help/contact', 'HelpController@contact')->name('help.contact');
    Route::get('/help/faq', 'HelpController@faq')->name('help.faq');
    Route::get('/help/ranks', 'HelpController@ranks')->name('help.ranks');
    Route::get('/help/levels', 'HelpController@levels')->name('help.levels');

    Route::get('/experience', 'ExperienceController@index')->name('experience.index');

    Route::get('/armory', 'ArmoryController@index')->name('armory.index');
});