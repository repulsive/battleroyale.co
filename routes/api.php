<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('/getExchangeRate', function (Request $request) {
    $amount = $request->amount;
    $currency = $request->currency;

    $return = 0;

    switch ($currency) {
        case "BRP":
            $return = $amount / 1000;
            break;

        case "BRC":
            $return = $amount * 1000;
            break;
    }

    return $return;
});